#include "../BaselineVarsBoostedAlg.h"
#include "../BaselineVarsResolvedAlg.h"
#include "../BaselineVarsyybbAlg.h"
#include "../BaselineVarsbbttAlg.h"
#include "../JetPairingAlg.h"
#include "../TruthParticleInformationAlg.h"

using namespace HH4B;

DECLARE_COMPONENT(BaselineVarsBoostedAlg)
DECLARE_COMPONENT(BaselineVarsResolvedAlg)
DECLARE_COMPONENT(BaselineVarsyybbAlg)
DECLARE_COMPONENT(BaselineVarsbbttAlg)
DECLARE_COMPONENT(JetPairingAlg)
DECLARE_COMPONENT(TruthParticleInformationAlg)
